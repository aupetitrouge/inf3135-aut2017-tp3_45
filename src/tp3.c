#include "application.h"
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

int main(int argc, char* argv[]) {
    struct Application *application = Application_initialize();
    if (application) {
        Application_run(application);
    } else {
        fprintf(stderr, "Failed to initialize the application.");
        return -1;
    }
	Application_shutDown(application);
	return 0;
}
